Cube
====

This is the result of my experimentation with computer graphics. Wanted to learn how linear algebra is used to rotate and move 3D objects and how z-buffering and texture mapping works. Object files (.obj) can be loaded and displayed.


Compiling
---------
This application requires CMake and SDL2 to compile. To install SDL2, take the following steps:

	Linux: Install the cmake, libsdl2 and libsdl2-dev packages.
	MacOS: Download the CMake source from https://cmake.org/ and the SDL2 source code from https://www.libsdl.org/. Install them both by following the instructions within those packages.

Use the following commands to compile the cube demo:

	mkdir build
	cd build
	cmake ..
	make


Usage
-----
Use an object file as a command line parameter for the application. A numeric value as a command line parameter overrides the default refresh rate.

Use the keys Up and Down to rotate the object, + and - to zoom in and out and Spacebar for pauze. Escape quits the application.
