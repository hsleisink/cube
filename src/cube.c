#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include "display.h"
#include "graphics.h"
#include "object.h"
#include "texture.h"

static int refresh_rate = 60;
static t_object *object;
t_texture *texture;

void init_cube(char *file) {
	int s;
	double cube_surfaces[12][3][3] = {
		// Back
		{ { 1,0,1 }, { 0,1,1 }, { 0,0,1 } },
		{ { 1,0,1 }, { 1,1,1 }, { 0,1,1 } },
		// Top
		{ { 0,1,0 }, { 1,1,1 }, { 1,1,0 } },
		{ { 0,1,0 }, { 0,1,1 }, { 1,1,1 } },
		// Bottom 
		{ { 0,0,1 }, { 1,0,0 }, { 1,0,1 } },
		{ { 0,0,1 }, { 0,0,0 }, { 1,0,0 } },
		// Left
		{ { 0,0,1 }, { 0,1,0 }, { 0,0,0 } },
		{ { 0,0,1 }, { 0,1,1 }, { 0,1,0 } },
		// Right
		{ { 1,0,0 }, { 1,1,1 }, { 1,0,1 } },
		{ { 1,0,0 }, { 1,1,0 }, { 1,1,1 } },
		// Front
		{ { 0,0,0 }, { 1,1,0 }, { 1,0,0 } },
		{ { 0,0,0 }, { 0,1,0 }, { 1,1,0 } }
	};
	unsigned long cube_colors[6] = { 0x0000FF, 0xFFFF00, 0x00FF00, 0xFFFFFF, 0xFF8000, 0xFF0000 };
	double max;

	if (file == NULL) {
		object = new_object(12);
		for (s = 0; s < 12; s++) {
			add_object_surface(object, cube_surfaces[s][0], cube_surfaces[s][1], cube_surfaces[s][2], cube_colors[s / 2]);
		}

		resize_object(object, 200, 200, 200);
		place_object(object, 0, 0, 0);
		rotate_object(object, -0.4, 0, 0.2);

		set_object_properties(object, ILLUMINATED | SHADED);

		if ((texture = load_texture("../resources/texture.bmp", 0, 0, 511, 511, 511, 0)) == NULL) {
			printf("Error loading texture.\n");
		} else {
			add_texture_to_object(object, texture, 6);
			add_texture_to_object(object, texture, 8);
		}

		if ((texture = load_texture("../resources/texture.bmp", 0, 0, 0, 511, 511, 511)) == NULL) {
			printf("Error loading texture.\n");
		} else {
			add_texture_to_object(object, texture, 7);
			add_texture_to_object(object, texture, 9);
		}
	} else { 
		if ((object = read_object(file)) == NULL) {
			exit(EXIT_FAILURE);
		}

		max = object->mx;
		if (object->my > max) max = object->my;
		if (object->mz > max) max = object->mz;
		max = sqrt(500.0 / max);

		resize_object(object, max, max, max);
		place_object(object, 0, 0, 0);

		set_object_properties(object, ILLUMINATED);
	}

	draw_objects();
}

void draw_cube(const char *key) {
	static double counter = 0, pause = 1;
	double x, z;

	if (key != NULL) {
		x = z = 0;
		if (strcmp(key, "Up") == 0) {
			x = -0.1;
		} else if (strcmp(key, "Down") == 0) {
			x = 0.1;
		} else if (strcmp(key, "-") == 0) {
			z = 20;
		} else if (strcmp(key, "=") == 0) {
			z = -20;
		} else if (strcmp(key, "Space") == 0) {
			pause = 1 - pause;
		}

		rotate_object(object, x, 0, 0);
		if (z != 0) {
			move_object(object, 0, 0, z);
		}
	} else {
		rotate_object(object, 0, -pause / refresh_rate, 0);

		counter += 0.3 / refresh_rate;

		draw_objects();
	}
}

int main(int argc, char *argv[]) {
	int rate, a;
	char *file = NULL;

	a = argc - 1;
	while (a > 0) {
		if ((rate = atoi(argv[a])) > 2) {
			refresh_rate = rate;
		} else {
			file = argv[a];
		}
		a--;
	}

	if (start_graphics(1024, 768, 0xCADEFF) == -1) {
		printf("Error opening display.\n");
		exit(EXIT_FAILURE);
	}

	init_cube(file);
	run_application(draw_cube, refresh_rate);

	stop_graphics();

	return EXIT_SUCCESS;
}
