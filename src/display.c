#include <stdio.h>
#include <unistd.h>
#include <SDL2/SDL.h>

SDL_Renderer *renderer;
SDL_Window *window;
static int screen_x, screen_y;
static unsigned long bg_color;

int init_display(int width, int height, unsigned long background_color) {
	SDL_Init(SDL_INIT_VIDEO);
	window = SDL_CreateWindow("Cube demo", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, 0);
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

	screen_x = width;
	screen_y = height;
	bg_color = background_color;

	return 0;
}

void close_display(void) {
	SDL_DestroyWindow(window);
	SDL_Quit();
}

void draw_point(int x, int y, unsigned long color) {
	SDL_SetRenderDrawColor(renderer, (color >> 16) & 255, (color >> 8) & 255, color & 255, 0);
	SDL_RenderDrawPoint(renderer, x, y);
}

void run_application(void (*application)(const char *), int refresh_rate) {
	int quit = 0;
	SDL_Event event;
	const char *key;

	while (quit == 0) {
		SDL_SetRenderDrawColor(renderer, (bg_color >> 16) & 255, (bg_color >> 8) & 255, bg_color & 255, 0);
		SDL_RenderClear(renderer);

		if (SDL_WaitEventTimeout(&event, 1000 / refresh_rate) == 0) {
			application(NULL);
			SDL_RenderPresent(renderer);
		} else switch (event.type) {
			case SDL_QUIT:
				quit = 1;
				break;
			case SDL_KEYDOWN:
				key = SDL_GetScancodeName(event.key.keysym.scancode);
				if (strcmp(key, "Escape") == 0) {
					quit = 1;
				} else {
					application(key);
				}
				break;
		}
	}
}
