#ifndef _DISPLAY_H
#define _DISPLAY_H

int init_display(int width, int height, unsigned long background_color);
void close_display(void);
void draw_point(int x, int y, unsigned long color);
void run_application(void (*application)(const char *), int refresh_rate);

#endif
