#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <math.h>
#include "graphics.h"
#include "display.h"
#include "matrix.h"

#define W 800
#define CAM_Z -600
#define VOF 1.8

typedef struct {
	double x3d, y3d, z3d;
	int x2d, y2d;
	int edge_nr;
	double edge_pos;
} t_point;

typedef struct {
	t_point point[2];
	t_point *left, *right;
	int points_set;
} t_frame;

static int screen_x, screen_y;
static int *z_buffer;
static double shade_depth = 0;

static void clear_z_buffer(void) {
	int x, y;

	for (y = 0; y < screen_y; y++) {
		for (x = 0; x < screen_x; x++) {
			*(z_buffer + y * screen_x + x) = INT_MAX;
		}
	}
}

int start_graphics(int width, int height, unsigned long bg_color) {
	screen_x = width;
	screen_y = height;

	if ((z_buffer = (int*)malloc(width * height * sizeof(int))) == NULL) {
		return -1;
	}
	clear_z_buffer();

	return init_display(width, height, bg_color);
}

void stop_graphics(void) {
	close_display();
}

void reset_graphics(void) {
	clear_z_buffer();
}

void set_shade_depth(double depth) {
	shade_depth = depth;
}

unsigned long make_color(unsigned short red, unsigned short green, unsigned short blue) {
	return ((((unsigned long)red << 8) + (unsigned long)green) << 8) + (unsigned long)blue;
}

unsigned long change_color(unsigned long color, double change) {
	int r, g, b;

	r = (color >> 16) & 255;
	g = (color >> 8) & 255;
	b = color & 255;

	r += r * change;
	g += g * change;
	b += b * change;

	if (r < 0) r = 0;
	if (g < 0) g = 0;
	if (b < 0) b = 0;

	if (r > 255) r = 255;
	if (g > 255) g = 255;
	if (b > 255) b = 255;

	return make_color(r, g, b);
}

/* Stores, when applicable, information about an edge point in the list.
 */
static int set_frame_point(t_frame *frame, t_point *point) {
	t_point *backup;

	if (frame->points_set == 0) {
		memcpy(frame->left, point, sizeof(t_point));
		frame->points_set = 1;
		return 1;
	}

	if (frame->points_set == 1) {
		if (point->x2d < frame->left->x2d) {
			backup = frame->right;
			frame->right = frame->left;
			frame->left = backup;
			memcpy(frame->left, point, sizeof(t_point));
		} else {	
			memcpy(frame->right, point, sizeof(t_point));
		}
		frame->points_set = 2;
		return 1;
	}

	if (point->x2d < frame->left->x2d) {
		memcpy(frame->left, point, sizeof(t_point));
		return 1;
	} else if (point->x2d > frame->right->x2d) {
		memcpy(frame->right, point, sizeof(t_point));
		return 1;
	}

	return 0;
}

/* Returns the length of the 2D vector from (0,0) to (x,y).
 */
static double length(double x, double y) {
	return sqrt(pow(x, 2) + pow(y, 2));
}

/* Transforms a 3D line to a 2D line and calculates the partition of each point
 * on that 2D libe. In other words, gets a perspective view of a 2D line, where
 * points in the far distance are closer to each other that nearby points.
 */
static double *get_2d_partition(t_point *point1, t_point *point2, int max_pixels) {
	double dx, dy, dz, min_x, min_y, point[4], *result;
	t_matrix *edge, *projected;
	int i;

	if (max_pixels <= 1) {
		if ((result = (double*)malloc(sizeof(double))) == NULL) {
			return NULL;
		}

		*result = 0;

		return result;
	}

	dx = (point1->x3d - point2->x3d) / (max_pixels - 1);
	dy = (point1->y3d - point2->y3d) / (max_pixels - 1);
	dz = (point1->z3d - point2->z3d) / (max_pixels - 1);

	if ((edge = new_matrix(max_pixels, 4)) == NULL) {
		return NULL;
	}

	point[0] = point2->x3d;
	point[1] = point2->y3d;
	point[2] = point2->z3d;
	point[3] = W;

	for (i = 0; i < max_pixels; i++) {
		add_vector_to_matrix(edge, point);

		point[0] += dx;
		point[1] += dy;
		point[2] += dz;
	}

	if ((projected = project_3d_to_2d(edge, VOF, screen_x, screen_y)) == NULL) {
		free_matrix(edge);
		return NULL;
	}

	free_matrix(edge);

	if ((result = (double*)malloc(max_pixels * sizeof(double))) == NULL) {
		free_matrix(projected);
	}

	min_x = get_matrix_value(projected, 0, 0);
	min_y = get_matrix_value(projected, 0, 1);
	dx = length(get_matrix_value(projected, max_pixels - 1, 0) - min_x,
	            get_matrix_value(projected, max_pixels - 1, 1) - min_y);
	for (i = 0; i < max_pixels; i++) {
		if (dx == 0) {
			*(result + i) = i / max_pixels;
		} else {
			*(result + i) = length(get_matrix_value(projected, i, 0) - min_x,
			                       get_matrix_value(projected, i, 1) - min_y) / dx;
		}
	}

	free_matrix(projected);

	return result;
}

/* A frame contains information about the edges of a triangle, from a verticial
 * point of view. For each point on those edges, there is the 3D coordinate,
 * the 2D coordinate, the edge number and the how far that point is positioned
 * on the 2D edge, ranging from 0 (positioned on the first 2D vertex) to 1
 * (positioned on the last 2D vertex).
 */
static void set_frame_edge(t_frame *frame, t_point *point1, t_point *point2, int min_y, int max_y, int edge_nr) {
	int dx2d, dy2d, len, i;
	double s, ds, dx3d, dy3d, dz3d;
	int max_partitions;
	t_point point;
	double *edge_partition;

	edge_partition = get_2d_partition(point1, point2, max_y - min_y + 1);
	max_partitions = max_y - min_y;

	dx2d = point2->x2d - point1->x2d;
	dy2d = point2->y2d - point1->y2d;

	dx3d = point2->x3d - point1->x3d;
	dy3d = point2->y3d - point1->y3d;
	dz3d = point2->z3d - point1->z3d;

	memcpy(&point, point1, sizeof(t_point));
	point.edge_nr = edge_nr;
	point.edge_pos = 0;

	if ((dx2d == 0) && (dy2d == 0)) {
		set_frame_point(frame + point.y2d - min_y, &point);
	} else if (abs(dx2d) > abs(dy2d)) {
		len = abs(dx2d);
		dx2d = (double)dx2d / len;
		s = point.y2d;
		ds = (double)dy2d / len;
		dx3d = dx3d / len;
		dy3d = dy3d / len;
		dz3d = dz3d / len;

		for (i = 0; i <= len; i++) {
			point.edge_pos = *(edge_partition + (int)(max_partitions * ((double)i / len)));
			set_frame_point(frame + (int)(s - min_y), &point);

			point.x2d += dx2d;
			s += ds;
			point.y2d = (int)s;
			point.x3d += dx3d;
			point.y3d += dy3d;
			point.z3d += dz3d;
		}
	} else {
		len = abs(dy2d);
		s = point.x2d;
		ds = (double)dx2d / len;
		dy2d = (double)dy2d / len;
		dx3d = dx3d / len;
		dy3d = dy3d / len;
		dz3d = dz3d / len;

		for (i = 0; i <= len; i++) {
			point.edge_pos = *(edge_partition + (int)(max_partitions * ((double)i / len)));
			set_frame_point(frame + point.y2d - min_y, &point);

			s += ds;
			point.x2d = (int)s;
			point.y2d += dy2d;
			point.x3d += dx3d;
			point.y3d += dy3d;
			point.z3d += dz3d;
		}
	}
}

/* Draws a 3D triangle.
 */
void draw_triangle(double vertex1[4], double vertex2[4], double vertex3[4], unsigned long color,
                   unsigned short mode, t_texture *texture) {
	double dz, z, da, a1, a2, *line_partition;
	t_point point1, point2, point3;
	int min_y, max_y, x, y, c, i, size;
	static t_frame *frames = NULL, *frame;
	static int frame_size = 0;
	static t_matrix *matrix = NULL;
	t_matrix *normal, *projected;
	unsigned long *line = NULL;

	if (matrix != NULL) {
		empty_matrix(matrix);
	} else if ((matrix = new_matrix(3, 4)) == NULL) {
		return;
	}

	vertex1[3] = vertex2[3] = vertex3[3] = W;

	add_vector_to_matrix(matrix, vertex1);
	add_vector_to_matrix(matrix, vertex2);
	add_vector_to_matrix(matrix, vertex3);

	if ((mode & MODE_ILLUMINATED) > 0) {
		if ((normal = normal_vector(vertex1, vertex2, vertex3)) != NULL) {
			color = change_color(color, 0.4 * get_matrix_value(normal, 0, 0) +
			                            0.2 * get_matrix_value(normal, 0, 1));
			free_matrix(normal);
		}
	}

	if ((projected = project_3d_to_2d(matrix, VOF, screen_x, screen_y)) == NULL) {
		return;
	}

	point1.x3d = vertex1[0];
	point1.y3d = vertex1[1];
	point1.z3d = vertex1[2];
	point1.x2d = get_matrix_value(projected, 0, 0);
	point1.y2d = get_matrix_value(projected, 0, 1);

	point2.x3d = vertex2[0];
	point2.y3d = vertex2[1];
	point2.z3d = vertex2[2];
	point2.x2d = get_matrix_value(projected, 1, 0);
	point2.y2d = get_matrix_value(projected, 1, 1);

	point3.x3d = vertex3[0];
	point3.y3d = vertex3[1];
	point3.z3d = vertex3[2];
	point3.x2d = get_matrix_value(projected, 2, 0);
	point3.y2d = get_matrix_value(projected, 2, 1);

	free_matrix(projected);

	if ((mode & MODE_DOUBLE_SIDED) == 0) {
		// Triangle facing backwards?
		a1 = atan2((double)(point2.x2d - point1.x2d), (double)(point2.y2d - point1.y2d));
		if (a1 < 0.0) {
			a1 += 2 * M_PI;
		}

		a2 = atan2((double)(point3.x2d - point2.x2d), (double)(point3.y2d - point2.y2d));
		if (a2 < 0.0) {
			a2 += 2 * M_PI;
		}

		if ((da = a2 - a1) < 0) {
			da += 2 * M_PI;
		}

		if (da >= M_PI) {
			return;
		}
	}

	min_y = point1.y2d;
	if (point2.y2d < min_y) min_y = point2.y2d;
	if (point3.y2d < min_y) min_y = point3.y2d;

	max_y = point1.y2d;
	if (point2.y2d > max_y) max_y = point2.y2d;
	if (point3.y2d > max_y) max_y = point3.y2d;

	size = (max_y - min_y + 1);
	if (size > frame_size) {
		if ((frames = (t_frame*)realloc(frames, size * sizeof(t_frame))) == NULL) {
			frame_size = 0;
			return;
		}
		frame_size = size;
	}

	memset(frames, 0, frame_size * sizeof(t_frame));
	for (i = 0; i < frame_size; i++) {
		(frames + i)->left = &((frames + i)->point[0]);
		(frames + i)->right = &((frames + i)->point[1]);
		(frames + i)->left->edge_nr = -1;
		(frames + i)->right->edge_nr = -1;
	}

	set_frame_edge(frames, &point1, &point2, min_y, max_y, 0);
	set_frame_edge(frames, &point2, &point3, min_y, max_y, 1);
	set_frame_edge(frames, &point3, &point1, min_y, max_y, 2);

	for (y = min_y; y <= max_y; y++) {
		if ((y < 0) || (y >= screen_y)) {
			continue;
		}

		frame = frames + (y - min_y);

		z = frame->left->z3d;
		dz = (frame->right->z3d - z) / (frame->right->x2d - frame->left->x2d + 1);

		if (texture != NULL) {
			line_partition = get_2d_partition(frame->left, frame->right, frame->right->x2d - frame->left->x2d + 1);
			line = get_texture_line(texture, frame->left->edge_nr, frame->left->edge_pos,
			                                 frame->right->edge_nr, frame->right->edge_pos,
			                                 frame->right->x2d - frame->left->x2d + 1,
			                                 line_partition);
		} else {
			line = NULL;
		}

		for (x = frame->left->x2d; x <= frame->right->x2d; x++) {
			if ((x < 0) || (x >= screen_x)) {
				z += dz;
				continue;
			}

			if (line != NULL) {
				c = line[x - frame->left->x2d];
			} else {
				c = color;
			}
			
			if ((mode & MODE_SHADED) > 0) {
				c = change_color(c, (shade_depth - z - 100) / 300);
			}

			i = y * screen_x + x;
			if ((int)z < *(z_buffer + i)) {
				if (z > CAM_Z) {
					draw_point(x, y, c);
				}
				*(z_buffer + i) = (int)z;
			}
			z += dz;
		}
	}
}
