#ifndef _GRAPHICS_H
#define _GRAPHICS_H

#define MODE_DOUBLE_SIDED 1
#define MODE_ILLUMINATED  2
#define MODE_SHADED       4

#include "texture.h"

int start_graphics(int width, int height, unsigned long bg_color);
void stop_graphics(void);
void reset_graphics(void);
void set_shade_depth(double depth);
unsigned long change_color(unsigned long color, double change);
unsigned long make_color(unsigned short red, unsigned short green, unsigned short blue);
void draw_triangle(double vertex1[4], double vertex2[4], double vertex3[4], unsigned long color, unsigned short mode, t_texture *texture);

#endif
