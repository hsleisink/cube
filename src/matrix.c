#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "matrix.h"

t_matrix *new_matrix(int x, int y) {
	t_matrix *matrix;

	if ((matrix = (t_matrix*)malloc(sizeof(t_matrix))) == NULL) {
		return NULL;
	}

	if ((matrix->values = (double*)malloc(x * y * sizeof(double))) == NULL) {
		free(matrix);
		return NULL;
	}

	matrix->x = x;
	matrix->y = y;
	empty_matrix(matrix);

	return matrix;
}

int add_vector_to_matrix(t_matrix *matrix, const double values[]) {
	int y;

	if (matrix->vectors_set + 1 > matrix->x) {
		return -1;
	}

	for (y = 0; y < matrix->y; y++) {
		*(matrix->values + (matrix->x * y) + matrix->vectors_set) = values[y];
	}

	matrix->vectors_set += 1;

	return 0;
}

int add_row_to_matrix(t_matrix *matrix, ...) {
	va_list ap;
	int x;

	if (matrix->rows_set + 1 > matrix->y) {
		return -1;
	}

	va_start(ap, matrix);
	for (x = 0; x < matrix->x; x++) {
		*(matrix->values + (matrix->x * matrix->rows_set) + x) = va_arg(ap, double);
	}
	va_end(ap);

	matrix->rows_set += 1;

	return 0;
}

void empty_matrix(t_matrix *matrix) {
	matrix->vectors_set = matrix->rows_set = 0;
	memset(matrix->values, 0, matrix->x * matrix->y * sizeof(double));
}

int set_matrix_value(t_matrix *matrix, int x, int y, double value) {
	if ((x < 0) || (x >= matrix->x) || (y < 0) || (y >= matrix->y)) {
		return -1;
	}

	*(matrix->values + (matrix->x * y) + x) = value;

	return 0;
}

double get_matrix_value(t_matrix *matrix, int x, int y) {
	if ((x < 0) || (x >= matrix->x) || (y < 0) || (y >= matrix->y)) {
		return 0;
	}

	return *(matrix->values + (matrix->x * y) + x);
}

t_matrix *multiply_matrix(t_matrix *matrix1, t_matrix *matrix2) {
	t_matrix *result;
	int x, y, s;

	if (matrix1->x != matrix2->y) {
		return NULL;
	}

	if ((result = new_matrix(matrix2->x, matrix1->y)) == NULL) {
		return NULL;
	}

	for (y = 0; y < result->y; y++) {
		for (x = 0; x < result->x; x++) {
			for (s = 0; s < matrix1->x; s++) {
				*(result->values + y * result->x + x) +=
					*(matrix1->values + y * matrix1->x + s) *
					*(matrix2->values + s * matrix2->x + x);
			}
		}
	}

	return result;
}

void free_matrix(t_matrix *matrix) {
	free(matrix->values);
	free(matrix);
}

t_matrix *normal_vector(double p1[4], double p2[4], double p3[4]) {
	t_matrix *normal;
	double x1, y1, z1, x2, y2, z2, nx, ny, nz, vector[3], len;

	if ((normal = new_matrix(1, 3)) == NULL) {
		return NULL;
	}

	x1 = p1[0] - p3[0];
	y1 = p1[1] - p3[1];
	z1 = p1[2] - p3[2];

	x2 = p2[0] - p3[0];
	y2 = p2[1] - p3[1];
	z2 = p2[2] - p3[2];

	nx = y1 * z2 - z1 * y2;
	ny = z1 * x2 - x1 * z2;
	nz = x1 * y2 - y1 * x2;

	if ((len = sqrt(nx * nx + ny * ny + nz * nz)) == 0) {
		free_matrix(normal);
		return NULL;
	}

	vector[0] = nx / len;
	vector[1] = ny / len;
	vector[2] = nz / len;

	if (add_vector_to_matrix(normal, vector) == -1) {
		free_matrix(normal);
		return NULL;
	}

	return normal;
}

t_matrix *project_3d_to_2d(t_matrix *matrix, double vof, int screen_x, int screen_y) {
	t_matrix *project, *projected;
	double aspect_ratio, pv1, pv2;
	int i;

	if ((project = new_matrix(4, 4)) == NULL) {
		return NULL;
	}

	aspect_ratio = (double)screen_y / screen_x;
	pv1 = vof * (screen_x >> 1) * aspect_ratio;
	pv2 = vof * (screen_y >> 1);

	add_row_to_matrix(project, pv1, 0.0, 0.0, 0.0);
	add_row_to_matrix(project, 0.0, pv2, 0.0, 0.0);
	add_row_to_matrix(project, 0.0, 0.0, 1.0, 1.0);
	add_row_to_matrix(project, 0.0, 0.0, 0.0, 1.0);

	if ((projected = multiply_matrix(project, matrix)) == NULL) {
		return NULL;
	}

	free_matrix(project);

	for (i = 0; i < projected->x; i++) {
		set_matrix_value(projected, i, 0, (get_matrix_value(projected, i, 0) / get_matrix_value(projected, i, 2)) + (screen_x >> 1));
		set_matrix_value(projected, i, 1, (get_matrix_value(projected, i, 1) / get_matrix_value(projected, i, 2)) + (screen_y >> 1));
		set_matrix_value(projected, i, 2, get_matrix_value(matrix, i, 2));
	}

	return projected;
}

void print_matrix(t_matrix *matrix) {
	int y, x;

	if (matrix == NULL) {
		printf("NULL matrix.\n");
		return;
	}

	for (y = 0; y < matrix->y; y++) {
		printf("-");
		for (x = 0; x < matrix->x; x++) {
			printf(" %6.2f", *(matrix->values + y * matrix->x + x));
		}
		printf("\n");
	}
	printf("\n");
}
