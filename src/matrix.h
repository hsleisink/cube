#ifndef _MATRIX_H
#define _MATRIX_H

#include <stdarg.h>

typedef struct {
	int x, y, vectors_set, rows_set;
	double *values;
} t_matrix;

t_matrix *new_matrix(int x, int y);
int add_vector_to_matrix(t_matrix *matrix, const double values[]);
int add_row_to_matrix(t_matrix *matrix, ...);
void empty_matrix(t_matrix *matrix);
int set_matrix_value(t_matrix *matrix, int x, int y, double value);
double get_matrix_value(t_matrix *matrix, int x, int y);
t_matrix *multiply_matrix(t_matrix *matrix1, t_matrix *matrix2);
void free_matrix(t_matrix *matrix);
t_matrix *normal_vector(double p1[4], double p2[4], double p3[4]);
t_matrix *project_3d_to_2d(t_matrix *matrix, double vof, int screen_x, int screen_y);
void print_matrix(t_matrix *matrix);

#endif
