#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <limits.h>
#include "object.h"
#include "graphics.h"

#define LAST_POINT INT_MIN

static t_object *objects = NULL;

static void determine_object_center(t_object *object) {
	double x, y, z, lx, ly, lz, hx, hy, hz;
	int s;

	lx = hx = get_matrix_value(object->surfaces, 0, 0);
	ly = hy = get_matrix_value(object->surfaces, 0, 1);
	lz = hz = get_matrix_value(object->surfaces, 0, 2);

	for (s = 1; s < object->number_of_surfaces; s++) {
		x = get_matrix_value(object->surfaces, s, 0);
		y = get_matrix_value(object->surfaces, s, 1);
		z = get_matrix_value(object->surfaces, s, 2);

		if (x < lx) lx = x;
		if (x > hx) hx = x;
		if (y < ly) ly = y;
		if (y > hy) hy = y;
		if (z < lz) lz = z;
		if (z > hz) hz = z;
	}

	object->cx = (hx + lx) / 2;
	object->cy = (hy + ly) / 2;
	object->cz = (hz + lz) / 2;
}

t_object *new_object(int number_of_surfaces) {
	t_object *object;
	int s;

	if ((object = (t_object*)malloc(sizeof(t_object))) == NULL) {
		return NULL;
	}

	if ((object->colors = (unsigned long*)malloc(number_of_surfaces * sizeof(unsigned long))) == NULL) {
		free(object);
		return NULL;
	}

	if ((object->surfaces = new_matrix(number_of_surfaces * 3, 4)) == NULL) {
		free(object->colors);
		free(object);
		return NULL;
	}

	if ((object->texture = (t_texture**)malloc(number_of_surfaces * sizeof(t_texture*))) == NULL) {
		free(object->surfaces);
		free(object->colors);
		free(object);
		return NULL;
	}

	object->number_of_surfaces = number_of_surfaces;
	object->surfaces_set = 0;
	object->double_sided = false;
	object->illuminated = false;
	object->shaded = false;
	object->cx = object->cy = object->cz = 0;
	object->mx = object->my = object->mz = 0;

	for (s = 0; s < number_of_surfaces; s++) {
		object->texture[s] = NULL;
	}

	object->next = objects;
	objects = object;

	return object;
}

static int read_point(char **line) {
	int result;
	char *eon;

	while (**line == ' ') {
		(*line)++;
	}

	if ((**line == '\n') || (**line == '\0')) {
		return LAST_POINT;
	}

	eon = *line;
	while ((*eon != ' ') && (*eon != '\n') && (*eon != '\0')) {
		if (*eon == '\\') {
			*eon = '\0';
		}
		eon++;
	}

	if (*eon == ' ') {
		*eon = '\0';
		eon++;
	} else {
		*eon = '\0';
	}

	result = atoi(*line) - 1;

	*line = eon;

	return result;
}

t_object *read_object(char *file) {
	t_object *object;
	FILE *fp;
	char line[1024], *v, *eon;
	int number_of_points = 0, number_of_surfaces = 0, s = 0, p1, p2, p3;
	double *points, c1[3], c2[3], c3[3];
	unsigned long color = 0x808080;

	if ((fp = fopen(file, "r")) == NULL) {
		printf("Object file not found.\n");
		return NULL;
	}

	/* Determine object size
	 */
	line[1023] = '\0';
	while (fgets(line, 1023, fp) != NULL) {
		if (strncmp(line, "v ", 2) == 0) {
			number_of_points++;
		}
		if (strncmp(line, "f ", 2) == 0) {
			number_of_surfaces++;

			p1 = 0;
			v = line + 2;
			while (read_point(&v) != LAST_POINT) {
				p1++;
			}

			if (p1 < 3) {
				fclose(fp);
				return NULL;
			}

			number_of_surfaces += (p1 - 3);
		}
	}

	if ((number_of_points == 0) || (number_of_surfaces == 0)) {	
		fclose(fp);
		return NULL;
	}

	/* Allocate memory
	 */
	if ((points = (double*)malloc(number_of_points * 3 * sizeof(double))) == NULL) {
		fclose(fp);
		return NULL;
	}

	if ((object = new_object(number_of_surfaces)) == NULL) {
		fclose(fp);
		free(points);
		return NULL;
	}

	/* Read points
	 */
	fseek(fp, 0L, SEEK_SET);
	while (fgets(line, 1023, fp) != NULL) {
		if (strncmp(line, "v ", 2) != 0) {
			continue;
		}

		v = line + 2;
		if ((eon = strchr(v, ' ')) == NULL) {
			goto abort_read;
		}
		*eon = '\0';
		points[3 * s] = (double)atof(v);

		v = eon + 1;
		if ((eon = strchr(v, ' ')) == NULL) {
			goto abort_read;
		}
		*eon = '\0';
		points[3 * s + 1] = (double)atof(v);

		v = eon + 1;
		if ((eon = strchr(v, '\n')) == NULL) {
			goto abort_read;
		}
		*eon = '\0';
		points[3 * s + 2] = (double)atof(v);

		s++;
	}

	/* Read surfaces
	 */
	fseek(fp, 0L, SEEK_SET);
	while (fgets(line, 1023, fp) != NULL) {
		if (strncmp(line, "usemtl color_", 13) == 0) {
			v = line + 13;
			if ((eon = strchr(v, '\n')) == NULL) {
				*eon = '\0';
			}
			color = (unsigned long)atol(v);
			continue;
		}

		if (strncmp(line, "f ", 2) != 0) {
			continue;
		}

		v = line + 2;
		if ((p1 = read_point(&v)) == LAST_POINT) {
			goto abort_read;
		}
		if ((p2 = read_point(&v)) == LAST_POINT) {
			goto abort_read;
		}
		if ((p3 = read_point(&v)) == LAST_POINT) {
			goto abort_read;
		}

		do {
			if ((p1 > number_of_points) || (p2 > number_of_points) || (p3 > number_of_points)) {
				goto abort_read;
			}

			c1[0] = points[3 * p1];
			c1[1] = points[3 * p1 + 1];
			c1[2] = points[3 * p1 + 2];

			c2[0] = points[3 * p2];
			c2[1] = points[3 * p2 + 1];
			c2[2] = points[3 * p2 + 2];

			c3[0] = points[3 * p3];
			c3[1] = points[3 * p3 + 1];
			c3[2] = points[3 * p3 + 2];

			if (add_object_surface(object, c1, c2, c3, color) == -1) {	
				goto abort_read;
			}

			p2 = p3;
			p3 = read_point(&v);
		} while (p3 != LAST_POINT);
	}

	goto done_read;

abort_read:
	free(object);
	object = NULL;

done_read:
	fclose(fp);
	free(points);

	return object;
}

void set_object_properties(t_object *object, unsigned int properties) {
	if ((properties & DOUBLE_SIDED) > 0) {
		object->double_sided = true;
	}

	if ((properties & ILLUMINATED) > 0) {
		object->illuminated = true;
	}

	if ((properties & SHADED) > 0) {
		object->shaded = true;
	}
}

void set_object_center(t_object *object, double x, double y, double z) {
	object->cx = x;
	object->cy = y;
	object->cz = z;
}

int add_object_surface(t_object *object, double c1[3], double c2[3], double c3[3], unsigned long color) {
	double vector[4];
	int c, s;
	double x, y, z;

	if (object->surfaces_set >= object->number_of_surfaces) {
		return -1;
	}

	vector[3] = 1.0;

	for (c = 0; c < 3; c++) {
		vector[c] = c1[c];
	}
	add_vector_to_matrix(object->surfaces, vector);

	for (c = 0; c < 3; c++) {
		vector[c] = c2[c];
	}
	add_vector_to_matrix(object->surfaces, vector);

	for (c = 0; c < 3; c++) {
		vector[c] = c3[c];
	}
	add_vector_to_matrix(object->surfaces, vector);

	object->colors[object->surfaces_set] = color;

	(object->surfaces_set)++;

	if (object->surfaces_set == object->number_of_surfaces) {
		determine_object_center(object);

		for (s = 0; s < object->number_of_surfaces; s++) {
			x = fabs(get_matrix_value(object->surfaces, s, 0) - object->cx);
			y = fabs(get_matrix_value(object->surfaces, s, 1) - object->cy);
			z = fabs(get_matrix_value(object->surfaces, s, 2) - object->cz);

			if (x > object->mx) object->mx = x;
			if (y > object->my) object->my = y;
			if (z > object->mz) object->mz = z;
		}
	}

	return 0;
}

int rotate_object(t_object *object, double dx, double dy, double dz) {
	t_matrix *rotate, *rotated;
	double cx, cy, cz, sr, cr;

	cx = object->cx;
	cy = object->cy;
	cz = object->cz;
	move_object(object, -cx, -cy, -cz);

	if ((rotate = new_matrix(4, 4)) == NULL) {
		return -1;
	}

	// Rotate X
	sr = sin(dx);
	cr = cos(dx);

	add_row_to_matrix(rotate, 1.0, 0.0, 0.0, 0.0);
	add_row_to_matrix(rotate, 0.0,  cr, -sr, 0.0);
	add_row_to_matrix(rotate, 0.0,  sr,  cr, 0.0);
	add_row_to_matrix(rotate, 0.0, 0.0, 0.0, 1.0);

	if ((rotated = multiply_matrix(rotate, object->surfaces)) == NULL) {
		free_matrix(rotate);
		return -1;
	}
	free_matrix(object->surfaces);
	object->surfaces = rotated;
	
	// Rotate Y
	sr = sin(dy);
	cr = cos(dy);

	empty_matrix(rotate);
	add_row_to_matrix(rotate,  cr, 0.0,  sr, 0.0);
	add_row_to_matrix(rotate, 0.0, 1.0, 0.0, 0.0);
	add_row_to_matrix(rotate, -sr, 0.0,  cr, 0.0);
	add_row_to_matrix(rotate, 0.0, 0.0, 0.0, 1.0);

	if ((rotated = multiply_matrix(rotate, object->surfaces)) == NULL) {
		free_matrix(rotate);
		return -1;
	}
	free_matrix(object->surfaces);
	object->surfaces = rotated;

	// Rotate Z
	sr = sin(dz);
	cr = cos(dz);

	empty_matrix(rotate);
	add_row_to_matrix(rotate,  cr, -sr, 0.0, 0.0);
	add_row_to_matrix(rotate,  sr,  cr, 0.0, 0.0);
	add_row_to_matrix(rotate, 0.0, 0.0, 1.0, 0.0);
	add_row_to_matrix(rotate, 0.0, 0.0, 0.0, 1.0);

	if ((rotated = multiply_matrix(rotate, object->surfaces)) == NULL) {
		free_matrix(rotate);
		return -1;
	}
	free_matrix(object->surfaces);
	object->surfaces = rotated;

	free_matrix(rotate);

	move_object(object, cx, cy, cz);

	return 0;
}

int move_object(t_object *object, double dx, double dy, double dz) {
	t_matrix *move, *moved;

	if ((move = new_matrix(4, 4)) == NULL) {
		return -1;
	}

	add_row_to_matrix(move, 1.0, 0.0, 0.0,  dx);
	add_row_to_matrix(move, 0.0, 1.0, 0.0,  dy);
	add_row_to_matrix(move, 0.0, 0.0, 1.0,  dz);
	add_row_to_matrix(move, 0.0, 0.0, 0.0, 1.0);

	if ((moved = multiply_matrix(move, object->surfaces)) == NULL) {
		free_matrix(move);
		return -1;
	}
	free_matrix(object->surfaces);
	object->surfaces = moved;

	free_matrix(move);

	object->cx += dx;
	object->cy += dy;
	object->cz += dz;

	return 0;
}

int place_object(t_object *object, double x, double y, double z) {
	double dx, dy, dz;

	dx = x - object->cx;
	dy = y - object->cy;
	dz = z - object->cz;

	return move_object(object, dx, dy, dz);
}

int resize_object(t_object *object, double dx, double dy, double dz) {
	t_matrix *resize, *resized;

	if ((resize = new_matrix(4, 4)) == NULL) {
		return -1;
	}

	add_row_to_matrix(resize,  dx, 0.0, 0.0, 0.0);
	add_row_to_matrix(resize, 0.0,  dy, 0.0, 0.0);
	add_row_to_matrix(resize, 0.0, 0.0,  dz, 0.0);
	add_row_to_matrix(resize, 0.0, 0.0, 0.0, 1.0);

	if ((resized = multiply_matrix(resize, object->surfaces)) == NULL) {
		free_matrix(resize);
		return -1;
	}
	free_matrix(object->surfaces);
	object->surfaces = resized;

	free_matrix(resize);

	object->cx *= dx;
	object->cy *= dy;
	object->cz *= dz;

	object->mx *= dx;
	object->my *= dy;
	object->mz *= dz;

	return 0;
}

int add_texture_to_object(t_object *object, t_texture *texture, int surface_nr) {
	object->texture[surface_nr] = texture;

	return 0;
}

void draw_objects(void) {
	t_object *object;
	int s, sp;
	double p1[4], p2[4], p3[4];
	unsigned short mode;

	object = objects;

	while (object != NULL) {
		set_shade_depth(object->cz);

		mode = (object->double_sided ? MODE_DOUBLE_SIDED : 0) | 
		       (object->illuminated ? MODE_ILLUMINATED : 0) |
		       (object->shaded ? MODE_SHADED : 0);

		for (s = 0; s < object->number_of_surfaces; s++) {
			sp = s * 3;

			p1[0] = get_matrix_value(object->surfaces, sp + 0, 0);
			p1[1] = get_matrix_value(object->surfaces, sp + 0, 1);
			p1[2] = get_matrix_value(object->surfaces, sp + 0, 2);
			p1[3] = get_matrix_value(object->surfaces, sp + 0, 3);

			p2[0] = get_matrix_value(object->surfaces, sp + 1, 0);
			p2[1] = get_matrix_value(object->surfaces, sp + 1, 1);
			p2[2] = get_matrix_value(object->surfaces, sp + 1, 2);
			p2[3] = get_matrix_value(object->surfaces, sp + 1, 3);

			p3[0] = get_matrix_value(object->surfaces, sp + 2, 0);
			p3[1] = get_matrix_value(object->surfaces, sp + 2, 1);
			p3[2] = get_matrix_value(object->surfaces, sp + 2, 2);
			p3[3] = get_matrix_value(object->surfaces, sp + 2, 3);

			draw_triangle(p1, p2, p3, object->colors[s], mode, object->texture[s]);
		}

		object = object->next;
	}

	reset_graphics();
}

void print_object(t_object *object) {
	print_matrix(object->surfaces);
	printf("Center: %f %f %f\n", object->cx, object->cy, object->cz);
	printf("Max   : %f %f %f\n\n", object->mx, object->my, object->mz);
}
