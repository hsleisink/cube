#ifndef _OBJECT_H
#define _OBJECT_H

#define DOUBLE_SIDED 1
#define ILLUMINATED  2
#define SHADED       4

#include <stdbool.h>
#include "matrix.h"
#include "texture.h"

typedef struct type_object {
	int id;
	int number_of_surfaces, surfaces_set;
	double cx, cy, cz, mx, my, mz;
	t_matrix *surfaces;
	unsigned long *colors;
	bool double_sided, illuminated, shaded;
	t_texture **texture;

	struct type_object *next;
} t_object;

t_object *new_object(int number_of_surfaces);
t_object *read_object(char *file);
void set_object_properties(t_object *object, unsigned int properties);
void set_object_center(t_object *object, double x, double y, double z);
int add_object_surface(t_object *object, double c1[3], double c2[3], double c3[3], unsigned long color);
int rotate_object(t_object *object, double dx, double dy, double dz);
int move_object(t_object *object, double dx, double dy, double dz);
int place_object(t_object *object, double x, double y, double z);
int resize_object(t_object *object, double dx, double dy, double dz);
int add_texture_to_object(t_object *object, t_texture *texture, int surface_nr);
void draw_objects(void);
void print_object(t_object *object);

#endif
