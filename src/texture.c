#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include <fcntl.h>
#include "texture.h"

t_texture *textures = NULL;

static unsigned long *line = NULL;
static int line_size = 0;

static t_texture *load_bitmap(char *file) {
	int fp, x, y, offset;
	struct stat status;
	unsigned char *data;
	t_texture *texture = NULL;

	if (stat(file, &status) == -1) {
		return NULL;
	}

	if ((fp = open(file, O_RDONLY)) == -1) {
		return NULL;
	}

	if ((data = (unsigned char*)malloc(status.st_size)) == NULL) {
		goto read_error;
	}

	if (read(fp, data, status.st_size) == -1) {
		goto read_error;
	}

	if (data[0] + (data[1] << 8) != 0x4D42) {
		printf("Wrong BMP signature.\n");
		goto read_error;
	}

	if (data[28] + (data[29] << 8) != 24) {
		printf("BMP not 24 bits color.\n");
		goto read_error;
	}

	x = data[18] + (data[19] << 8) + (data[20] << 16) + (data[21] << 24);
	y = data[22] + (data[23] << 8) + (data[24] << 16) + (data[25] << 24);

	if ((texture = (t_texture*)malloc(sizeof(t_texture))) == NULL) {
		goto read_error;
	}
	if ((texture->data = (unsigned long*)malloc(x * y * sizeof(unsigned long))) == NULL) {
		goto read_error;
	}

	texture->width = x;
	texture->height = y;

	for (y = 0; y < texture->height; y++) {
		for (x = 0; x < texture->width; x++) {
			offset = 3 * (texture->height - y - 1) * texture->width + 3 * x;
			texture->data[y * texture->width + x] = data[54 + offset] +
			                                       (data[54 + offset + 1] << 8) +
			                                       (data[54 + offset + 2] << 16);
		}
	}

	goto read_done;

read_error:
	if (texture != NULL) {
		if (texture->data != NULL) {
			free(texture->data);
		}
		free(texture);

		texture = NULL;
	}

read_done:
	if (data != NULL) {
		free(data);
	}

	close(fp);

	return texture;
}

t_texture *load_texture(char *file, int x1, int y1, int x2, int y2, int x3, int y3) {
	t_texture *texture, *existing;

	texture = textures;
	while (texture != NULL) {
		if (strcmp(texture->file, file) == 0) {
			break;
		}
	}

	if (texture == NULL) {
		if ((texture = load_bitmap(file)) == NULL) {
			return NULL;
		}
		if ((texture->file = strdup(file)) == NULL) {
			free(texture);
			return NULL;
		}
		
		texture->next = textures;
		textures = texture;
	} else {
		existing = texture;
		if ((texture = (t_texture*)malloc(sizeof(t_texture))) == NULL) {
			return NULL;
		}
		texture->file = existing->file;
		texture->width = existing->width;
		texture->height = existing->height;
		texture->data = existing->data;
	}

	if ((x1 < 0) || (y1 < 0) || (x2 < 0) || (y2 < 0) || (x3 < 0) || (y3 < 0)) {
	    printf("Texture outside boundaries.\n");
		return NULL;
	}

	if ((x1 >= texture->width) || (y1 >= texture->height) ||
	    (x2 >= texture->width) || (y2 >= texture->height) ||
	    (x3 >= texture->width) || (y3 >= texture->height)) {
        printf("Texture outside boundaries.\n");
		return NULL;
	}

	texture->p1[0] = x1;
	texture->p1[1] = y1;
	texture->p2[0] = x2;
	texture->p2[1] = y2;
	texture->p3[0] = x3;
	texture->p3[1] = y3;

	return texture;
}

static int get_edge_coord(t_texture *texture, int edge_nr, double edge_pos, double *x, double *y) {
	double ex, ey;

	switch (edge_nr) {
		case 0:
			*x = texture->p1[0];
			*y = texture->p1[1];
			ex = texture->p2[0];
			ey = texture->p2[1];
			break;
		case 1:
			*x = texture->p2[0];
			*y = texture->p2[1];
			ex = texture->p3[0];
			ey = texture->p3[1];
			break;
		case 2:
			*x = texture->p3[0];
			*y = texture->p3[1];
			ex = texture->p1[0];
			ey = texture->p1[1];
			break;
		default:
			return -1;
	}

	*x += (ex - *x) * edge_pos;
	*y += (ey - *y) * edge_pos;

	return 0;
}

static long get_texture_pixel(t_texture *texture, double x, double y) {
	int tx, ty, sx, sy, c, step;
	unsigned long color, red, green, blue;

	tx = (int)x;
	ty = (int)y;
	c = red = green = blue = 0;
	step = 0;

	if (step == 0) {
		if ((x < 0) || (y < 0) || (x >= texture->width) || (y >= texture->height)) {
			return -1;
		}

		return texture->data[(int)y * texture->width + (int)x];
	}

	for (sy = ty - step; sy <= ty + step; sy++) {
		for (sx = tx - step; sx <= tx + step; sx++) {
			if ((sx < 0) || (sy < 0) || (sx >= texture->width) || (sy >= texture->height)) {
				continue;
			}

			color = texture->data[sy * texture->width + sx];
			red += color >> 16;
			green += (color >> 8) & 255;
			blue += color & 255;

			c++;
		}
	}

	if (c == 0) {
		return -1;
	}

	return ((red / c) << 16) + ((green / c) << 8) + (blue / c);
}

unsigned long *get_texture_line(t_texture *texture, int edge_nr_1, double edge_pos_1, int edge_nr_2, double edge_pos_2, int len, double *edge) {
	double x1, y1, x2, y2, dx, dy, pos;
	long pixel;
	int i;

	if (len == 0) {
		return NULL;
	}

	if (len > line_size) {
		line_size = len + 100;
		if ((line = (unsigned long*)realloc(line, line_size * sizeof(unsigned long*))) == NULL) {
			line_size = 0;
			return NULL;
		}
	}

	if (get_edge_coord(texture, edge_nr_1, edge_pos_1, &x1, &y1) == -1) {
		return NULL;
	}
	if (get_edge_coord(texture, edge_nr_2, edge_pos_2, &x2, &y2) == -1) {
		return NULL;
	}

	dx = x2 - x1;
	dy = y2 - y1;

	for (i = 0; i < len; i++) {
		if (len > 1) {
			pos = *(edge + (int)((double)len * (double)i / len));
		} else {
			pos = 0;
		}

		if ((pixel = get_texture_pixel(texture, x1 + pos * dx, y1 + pos * dy)) == -1) {
			printf("Pixel outside texture.\n");
			*(line + i) = 0xFF0000;
		} else {
			*(line + i) = (unsigned long)pixel;
		}
	}

	return line;
}
