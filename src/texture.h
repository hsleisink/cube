#ifndef _TEXTURE_H
#define _TEXTURE_H

typedef struct type_texture {
	char *file;
	int width, height;
	unsigned long *data;
	int p1[2], p2[2], p3[2];

	struct type_texture *next;
} t_texture;

t_texture *load_texture(char *file, int x1, int y1, int x2, int y2, int x3, int y3);
unsigned long *get_texture_line(t_texture *texture, int edge_nr_1, double edge_pos_1, int edge_nr_2, double edge_pos_2, int len, double *edge);

#endif
